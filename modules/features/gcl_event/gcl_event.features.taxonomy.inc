<?php
/**
 * @file
 * gcl_event.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function gcl_event_taxonomy_default_vocabularies() {
  return array(
    'gcl_event_type' => array(
      'name' => 'Event type',
      'machine_name' => 'gcl_event_type',
      'description' => 'Types of event for this site. It will allow the users to filter by this types.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
