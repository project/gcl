<?php
/**
 * @file
 * gcl_news.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function gcl_news_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access service links'.
  $permissions['access service links'] = array(
    'name' => 'access service links',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'service_links',
  );

  return $permissions;
}
