<?php
/**
 * @file
 * gcl_site_configuration.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function gcl_site_configuration_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-banner_block-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'banner_block-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'gcl_gob_cl' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'gcl_gob_cl',
        'weight' => 0,
      ),
      'gcl_seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'gcl_seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-gcl_event_calendar-block_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'gcl_event_calendar-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'gcl_gob_cl' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'gcl_gob_cl',
        'weight' => 0,
      ),
      'gcl_seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'gcl_seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-news_list-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'news_list-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'gcl_gob_cl' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'gcl_gob_cl',
        'weight' => -8,
      ),
      'gcl_seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'gcl_seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-news_list-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'news_list-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'gcl_gob_cl' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'gcl_gob_cl',
        'weight' => -10,
      ),
      'gcl_seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'gcl_seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-service_list-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'service_list-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'gcl_gob_cl' => array(
        'region' => 'top_first',
        'status' => 1,
        'theme' => 'gcl_gob_cl',
        'weight' => 0,
      ),
      'gcl_seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'gcl_seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
