<?php
/**
 * @file
 * gcl_migrate_config.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gcl_migrate_config_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'migrate_disabled_handlers';
  $strongarm->value = 'a:1:{i:0;s:24:"MigrateImageFieldHandler";}';
  $export['migrate_disabled_handlers'] = $strongarm;

  return $export;
}
