<?php
/**
 * @file
 * Definitions for page migration.
 * 
 * All of this code is based on migrate module standard.
 */


class PageXMLMigration extends XMLMigration {

  public function __construct($arguments) {
    global $language;

    $lng = variable_get('gcl_import_language', $language->language);

    $arguments['group_name'] = 'gcl_content_example';
    parent::__construct($arguments);
    $this->description = t('XML feed (multi items) of pages');

    $fields = $this->getFields();

    $xml_folder = DRUPAL_ROOT . '/' .
                  drupal_get_path('module', 'gcl_content_example') . '/data/';
    $items_url = $xml_folder . 'page.xml';
    $item_xpath = '/pages/page';
    $item_ID_xpath = 'id';

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    $this->destination = new MigrateDestinationNode('page');
    $this->setMap2();


    $this->addFieldMapping('title', 'title')
         ->xpath('title/' . $lng);
    $this->addFieldMapping('title_field', 'title')
         ->xpath('title/' . $lng);

    $this->addFieldMapping('field_page_description', 'body')
         ->xpath('body/' . $lng);
    $this->addFieldMapping('field_page_description:format')->defaultValue('html');

    $this->addImages($lng);

    $this->addFieldMapping('field_related_content', 'relations')
         ->xpath('relations/relation/id_relation')
         ->sourceMigration(array('NewsXML', 'PageXML'));
  }






  public function complete($entity, stdClass $row) {
    $lng = variable_get('gcl_import_language', 'en');

    $menu_name = "main-menu";
    $menu_link = array(
      'menu_name' => $menu_name,
      'link_path' => "node/" . $entity->nid,
      'link_title' => (string) $row->xml->menu_link->title->{$lng},
    );

    menu_link_save($menu_link);
  }



  protected function addImages($lng) {

    $this->addFieldMapping('field_page_image', 'img_name')
      ->xpath('img/filename');

    /* In case of multilingual, this must to be changed. */
    $this->addFieldMapping('field_page_image:language')
      ->defaultValue(LANGUAGE_NONE);

    $this->addFieldMapping('field_page_image:source_dir')
      ->defaultValue(drupal_get_path('module', 'gcl_content_example') . '/images');

    $this->addFieldMapping('field_page_image:alt', 'img_alt')
      ->xpath('img/alt/' . $lng);

    $this->addFieldMapping('field_page_image:title', 'img_title')
      ->xpath('img/title/' . $lng);
    $this->addFieldMapping('field_page_image:center', 'img_center')
      ->xpath('img/center');
  }



  protected function createStub($migration, array $source_id) {
    $node = new stdClass();
    $node->title = t('Stub de tipo @type for @id', array('@type' => $this->destination->getBundle(), '@id' => $source_id[0]));
    $node->type = $this->destination->getBundle();
    $node->uid = 1;
    $node->status = 0;
    node_save($node);
    if (isset($node->nid)) {
      return array($node->nid);
    }
    else {
      return FALSE;
    }
  }


  protected function setMap2() {
      $this->map = new MigrateSQLMap($this->machineName,
      array(
        'sourceid' => array(
          'type' => 'varchar',
          'length' =>  64,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
  }


  protected function getFields() {
    $fields = array(
      'id'    => t('identifier'),
      'title' => t('Title'),
      'description'   => t('Description'),
      'image' => t('image file name'),
      'menu_link' => t('Menu link')
    );

    return $fields;
  }
}