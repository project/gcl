<?php
/**
 * @file
 * Definitions for banner migration.
 * 
 * All of this code is based on migrate module standard.
 */

class BannerXMLMigration extends XMLMigration {


  public function __construct($arguments) {
    global $language;

    $lng = variable_get('gcl_import_language', $language->language);

    $arguments['group_name'] = 'gcl_content_example';
    parent::__construct($arguments);
    $this->description = t('XML feed (multi items) of banners');

    $fields = $this->getFields();

    $xml_folder = DRUPAL_ROOT . '/' .
                  drupal_get_path('module', 'gcl_content_example') . '/data/';
    $items_url = $xml_folder . 'banner.xml';
    $item_xpath = '/banners/banner';
    $item_ID_xpath = 'id';

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    $this->destination = new MigrateDestinationNode('banner');
    $this->setMap2();


    $this->addFieldMapping('title', 'title')
         ->xpath('title/' . $lng);
    $this->addFieldMapping('title_field', 'title')
         ->xpath('title/' . $lng);

    $this->addLink($lng);

    $this->addImages($lng);
  }



  /**
   * Adds values for the product image.
   *
   */
  protected function addImages($lng) {
    $this->addFieldMapping('field_banner_image', 'img_name')
      ->xpath('img/filename');

    /* In case of multilingual, this must to be changed. */
    $this->addFieldMapping('field_banner_image:language')
      ->defaultValue(LANGUAGE_NONE);

    $this->addFieldMapping('field_banner_image:source_dir')
      ->defaultValue(drupal_get_path('module', 'gcl_content_example') . '/images');

    $this->addFieldMapping('field_banner_image:alt', 'img_alt')
      ->xpath('img/alt/' . $lng);

    $this->addFieldMapping('field_banner_image:title', 'img_title')
      ->xpath('img/title/' . $lng);


    $this->addFieldMapping('field_banner_image:center', 'img_center')
      ->xpath('img/center');
  }



  /**
   * Adds link values
   */
  protected function addLink($lng) {
     $this->addFieldMapping('field_banner_link:language')
      ->defaultValue(LANGUAGE_NONE);
     $this->addFieldMapping('field_banner_link:title', 'link_title')
      ->xpath('title/' . $lng);
     $this->addFieldMapping('field_banner_link', 'link_link')
      ->xpath('url/' . $lng);
  }




  protected function setMap2() {
      $this->map = new MigrateSQLMap($this->machineName,
      array(
        'sourceid' => array(
          'type' => 'varchar',
          'length' =>  64,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
  }


  protected function getFields() {
    $fields = array(
      'id'    => t('identifier'),
      'title' => t('Title'),
      'url'   => t('url'),
      'image' => t('image file name'),
    );

    return $fields;
  }
}
