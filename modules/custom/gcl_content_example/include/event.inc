<?php
/**
 * @file
 * Definitions for event migration.
 * 
 * All of this code is based on migrate module standard.
 */



class EventXMLMigration extends XMLMigration {

  public function __construct($arguments) {
    global $language;

    $lng = variable_get('gcl_import_language', $language->language);

    $arguments['group_name'] = 'gcl_content_example';
    parent::__construct($arguments);
    $this->description = t('XML feed (multi items) of events');

    $fields = $this->getFields();

    $xml_folder = DRUPAL_ROOT . '/' .
                  drupal_get_path('module', 'gcl_content_example') . '/data/';
    $items_url = $xml_folder . 'event.xml';
    $item_xpath = '/events/event';
    $item_ID_xpath = 'id';

    $items_class = new MigrateItemsXML($items_url, $item_xpath, $item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $fields);

    $this->destination = new MigrateDestinationNode('event');
    $this->setMap2();


    $this->addFieldMapping('title', 'title')
         ->xpath('title/' . $lng);
    $this->addFieldMapping('title_field', 'title')
         ->xpath('title/' . $lng);

    $this->addFieldMapping('field_event_description', 'description')
         ->xpath('description/' . $lng);
    $this->addFieldMapping('field_event_description:format')->defaultValue('html');

     $this->addFieldMapping('field_event_dates', 'dates');

    $this->addLink();

    $this->addImages($lng);
  }

  
  
  public function prepareRow($current_row) {
    $from_str = (string) $current_row->xml->dates->date_start;
    $to_str = (string) $current_row->xml->dates->date_end;

    $from = strtotime($from_str);
    $to = strtotime($to_str);

    $date_data = array(
      'from' => $from,
      'to'   => $to,
    );

    $current_row->dates = drupal_json_encode($date_data);
  }

  

  protected function addLink() {

  }
  
  
  
  protected function addImages($lng) {
    $this->addFieldMapping('field_event_image', 'img_name')
      ->xpath('img/filename');

    /* In case of multilingual, this must to be changed. */
    $this->addFieldMapping('field_event_image:language')
      ->defaultValue(LANGUAGE_NONE);

    $this->addFieldMapping('field_event_image:source_dir')
      ->defaultValue(drupal_get_path('module', 'gcl_content_example') . '/images');

    $this->addFieldMapping('field_event_image:alt', 'img_alt')
      ->xpath('img/alt/' . $lng);

    $this->addFieldMapping('field_event_image:title', 'img_title')
      ->xpath('img/title/' . $lng);

    $this->addFieldMapping('field_event_image:center', 'img_center')
      ->xpath('img/center');
  }

  protected function addTerm() {
    $name = "Other event type";
    $voc_name = "gcl_event_type";
    $term = taxonomy_get_term_by_name($name, $voc_name);

    $this->addFieldMapping('field_event_type')->defaultValue($term);

  }



  protected function setMap2() {
      $this->map = new MigrateSQLMap($this->machineName,
      array(
        'sourceid' => array(
          'type' => 'varchar',
          'length' =>  64,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
  }



  protected function getFields() {
    $fields = array(
      'id'    => t('identifier'),
      'title' => t('Title'),
      'description'   => t('Description'),
      'image' => t('image file name'),
      'dates' => t('Start and end date'),
    );

    return $fields;
  }
}