<?php
/**
 * @file
 * Declares a handler for focal_point migration.
 */

class ImageFocalPointValueFieldHandler extends MigrateImageFieldHandler {
  
  
  public function fields($type, $instance, $migration = NULL) {
    $fields = parent::fields($type, $instance, $migration);
    $fields += array(
      'center' => t('Image optic center'),
    );
    return $fields;
  }
  
  
  
  public function prepare($entity, array $field_info, array $instance, array $values) {
    $parent_field = parent::prepare($entity, $field_info, $instance, $values);
    
    if (isset($values['arguments']['center']) && is_numeric($parent_field['und'][0]['fid'])) {
      $image = file_load($parent_field['und'][0]['fid']);
      $image->focal_point = $values['arguments']['center'];
      file_save($image);
    }
    
    return $parent_field;
  }
}