<?php
/**
 * @file 
 * Declares hooks for migrate api to perform migrations.
 */

/* Implementation of hook_migrate_api(). */
function gcl_content_example_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'gcl_content_example' => array(
        'title' => t('GCL content example'),
      ),
    ),
    'migrations' => array(
      'NewsXML' => array(
        'class_name' => 'NewsXMLMigration',
        'group_name' => 'gcl_content_example',
      ),
      'BannerXML' => array(
        'class_name' => 'BannerXMLMigration',
        'group_name' => 'gcl_content_example',
      ),
      'EventXML' => array(
        'class_name' => 'EventXMLMigration',
        'group_name' => 'gcl_content_example',
      ),
      'PageXML' => array(
        'class_name' => 'PageXMLMigration',
        'group_name' => 'gcl_content_example',
      ),
    ),
    'field handlers' => array('ImageFocalPointValueFieldHandler')
  );

  return $api;
}