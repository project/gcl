<?php
/**
 * @file
 * Enables modules and site configuration for a picec site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function gcl_form_install_configure_form_alter(&$form, $form_state) {
  // This profile was designed to Chile.
  $form['server_settings']['site_default_country']['#default_value'] = 'CL';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'America/Santiago';
}