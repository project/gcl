<?php

/**
 * @file
 * Contains Batch API callbacks used during installation.
 */

/**
 * Install the gcl_demo module.
 */
function _gcl_install_modules_dependencies($module, $module_name, &$context) {
  module_enable(array($module), FALSE);

  $context['message'] = st('Installed %module module.', array('%module' => $module_name));
  $context['results'][] = $module;
}

/**
 * Finish modules.
 */
function _gcl_install_modules_flush_cache($success, $results, $operations) {
  drupal_flush_all_caches();
}



/**
 * Import the products.
 */
function _gcl_install_demo_import($machine_name, &$context) {
  $migration = Migration::getInstance($machine_name);
  $migration->processImport();

  $context['message'] = st('Importing Contents');
}


/**
 * Set up the banner and the logo for demo.
 */
function _gcl_install_demo_other(&$context) {
  $context['message'] = st('Set a Banner and a Logo');

  $name = 'theme_gcl_gob_cl_settings';
  $settings = variable_get($name, array());

  $settings['default_logo'] = 0;
  $settings['logo_path'] = drupal_get_path('theme', 'gcl_seven') . '/images/logo-example.png';
  $settings['toggle_logo'] = 1;
  $settings['gob_cl_header_image_default'] = 0;
  $settings['gob_cl_header_image_path'] = drupal_get_path('theme', 'gcl_seven') . '/images/banner-example.jpg';

  variable_set($name, $settings);
}
