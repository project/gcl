# Require any additional compass plugins installed on your system.
require 'breakpoint'
require 'susy'
require 'sass-globbing'


# Location of the theme's resources.
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
generated_images_dir = images_dir + "/generated"
javascripts_dir = "js"
