<?php

function gcl_gob_cl_preprocess_page(&$variables) {
  if (!empty($variables['node']) && !empty($variables['node']->type) && ( is_null(arg(2)) || arg(2) == 'view') ) {
    $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
  }
}