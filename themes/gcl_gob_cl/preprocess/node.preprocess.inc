<?php

function gcl_gob_cl_preprocess_node(&$vars){
  if($vars['view_mode'] === "teaser"){
    $vars['theme_hook_suggestions'][] = 'node__teaser';
    $vars['theme_hook_suggestions'][] = 'node__teaser__' . $vars['node']->type;
    
  }
  if ( $vars['node']->type == 'service' ) {
      
    // Color field
    $field_color = field_get_items('node', $vars['node'], 'field_service_color');
    $vars['bg_service_color'] = '#FFFFFF';
    if ($field_color) {
      $vars['bg_service_color'] = $field_color[0]['rgb'];
    }

    // Field Image
    $field_image = field_get_items('node', $vars['node'], 'field_service_image');
    $vars['bg_service_image'] = FALSE;
    if ($field_image) {
      $vars['bg_service_image'] = file_create_url($field_image[0]['uri']);
    }

    // Links
    $field_link = field_get_items('node', $vars['node'], 'field_service_link');
    $vars['service_link'] = $field_link[0]['url'];
  }

}