<?php


/**
 * Override or insert variables into the maintenance page template.
 */
function gcl_seven_preprocess_maintenance_page(&$vars) {
  $vars['features_content'] = FALSE;
  if (MAINTENANCE_MODE == 'install' && isset($_GET['id']) && $_GET['id'] && isset($_GET['op']) && $_GET['op'] == 'start') {

    $vars['features_content'] = _gcl_seven_features_content_slide();
  }

  $path = drupal_get_path('theme', 'gcl_seven');
  $image = theme('image', array('path' => file_create_url($path . '/images/tifon-logo-hor.png')));
  $options = array(
    'html' => TRUE,
    'attributes' => array(
      'title' => st('TIFON site'),
      'target' => '_blank',
    ),
  );
  $vars['logo_tifon'] = l($image, 'http://tifon.cl', $options);
}


/**
 * Create the content of the slide.
 */
function _gcl_seven_features_content_slide() {
  $path = drupal_get_path('theme', 'gcl_seven');

  drupal_add_js($path . '/js/features_content.js');
  drupal_add_css($path . '/css/features_content.css');

  $icon_1 = file_create_url($path . '/images/icon-1.png');
  $alt_icon_1 = st('Accessibility Standard');
  $title_1 = st('Accessibility Standard');
  $content_1 = st('Designed for most people regardless of their abilities or conditions.');

  $icon_2 = file_create_url($path . '/images/icon-2.png');
  $alt_icon_2 = st('SEO');
  $title_2 = st('Good SEO?');
  $content_2 = st('Want to be found anywhere? This is properly configured to do that.');

  $icon_3 = file_create_url($path . '/images/icon-3.png');
  $alt_icon_3 = st('Responsive');
  $title_3 = st('Responsive');
  $content_3 = st('Be visited from any device is a fact thanks to responsive design.');

  $output = '<div id="slideshow">';
  $items = array();
  $items[] = _gcl_seven_features_content_slide_item($icon_1, $alt_icon_1, $title_1, $content_1);
  $items[] = _gcl_seven_features_content_slide_item($icon_2, $alt_icon_2, $title_2, $content_2);
  $items[] = _gcl_seven_features_content_slide_item($icon_3, $alt_icon_3, $title_3, $content_3);
  shuffle($items);
  $output .= implode('', $items);
  $output .= '</div>';

  return $output;
}


/**
 * Prepare an item of the slide.
 * @param string $icon
 *  The path of the icon.
 * @param string $alt_icon
 *  The ALT attribute of the icon.
 * @param string $title
 *  The title of the slide.
 * @param string $content
 *  The content of the slide.
 *
 * @return string
 *  The html of one item.
 */
function _gcl_seven_features_content_slide_item($icon, $alt_icon, $title, $content) {
  $output = '';
  $output .= '<div class="slide">';
  $output .= theme('image', array('path' => $icon, 'alt' => $alt_icon, 'attributes' => array('class' => array('slide-icon slide-icon-1'))));
  $output .= '<div class="slide-content">';
  $output .= '<h2>' . $title . '</h2>';
  $output .= '<p>' . $content . '</p>';
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

