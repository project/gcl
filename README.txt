________________________________________________________________________________
|
|    GCL installation profile
|_______________________________________________________________________________

This profile intends to be an alternative for chilean government's sites.

It was based on "Kit Digital" which was published by the chilean government in
order to have an standard for its web sites. The original "Kit Digital" did not
have a Drupal alternative, just plain HTML and a WordPress theme.

This profile tries to best the original html files, by enhancing normal features
as such as SEO improvement and better accesibility capabilities. Besides that,
there are the normal Drupal capabilities that would allow the developers to
extend the core functionality for their own needs.

 _________________
|                 |
|  Installation   |
| _______________ |

It can be installed as a normal Drupal profile, with few options.

The custom options can be separated in 2 main areas: configuration and content.

Configuration:
--------------
The user can decide whether to configure the site with production
environment features (like cache peges) or dev environment features.
You will have the following options:
- Cache pages for anonymous users : as in Drupal performance configuration
- Aggregate and compress CSS files : as in Drupal performance configuration
- Aggregate JavaScript files : as in Drupal performance configuration
- Enable modules for building : Enable ui modules: views_ui, features_ui,
field_ui and migrate_ui.

Content:
---------
    The user can decide whether he wants example content for his site.
You will have the following options:
 - I want to install the demo : Installs the demo content. Note that if you
do not have migrate_ui activated, you would need to delete all those contents
manually.